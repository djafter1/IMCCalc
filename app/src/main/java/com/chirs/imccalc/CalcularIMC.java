package com.chirs.imccalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CalcularIMC extends AppCompatActivity {

    private TextView mensaje;
    private ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular_imc);

        mensaje = (TextView)findViewById(R.id.tvMensaje);
        imagen = (ImageView)findViewById(R.id.ivImagen);

        Bundle bd = getIntent().getExtras();

        float peso = bd.getFloat("ps");
        float altura = bd.getFloat("at");

        float res = peso / (altura * altura);
        String msg="", tmp="";

        if(res < 18.5){
            imagen.setImageResource(R.drawable.panterarosa);
            tmp = "bajo peso";
        }else if(res >= 18.5 && res < 25){
            imagen.setImageResource(R.drawable.goku);
            tmp = "peso normal";
        }else if(res >= 25){
            imagen.setImageResource(R.drawable.buu);
            tmp = "sobre peso";
        }

        msg = "Usted tiene "+ tmp;
        mensaje.setText(msg);
    }
}
