package com.chirs.imccalc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText peso, altura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        peso = (EditText)findViewById(R.id.etPeso);
        altura = (EditText)findViewById(R.id.etAltura);
    }

    public void calcular(View v){
        String pesos = peso.getText().toString();
        String alturas = altura.getText().toString();

        if(!validarPeso(pesos)){
            peso.setError("Debe indicar el peso");
        }else if(!validarAltura(alturas)){
            altura.setError("Debe indicar la altura");
        }else if(validarPeso(pesos) && validarAltura(alturas)){
            float ps = Float.parseFloat(pesos);
            float at = Float.parseFloat(alturas);

            Intent i = new Intent(this, CalcularIMC.class);
            i.putExtra("ps", ps);
            i.putExtra("at", at);
            startActivity(i);
        }
    }

    private boolean validarPeso(String ps){
        if(!ps.isEmpty()){
            return true;
        }else{
            return false;
        }
    }

    private boolean validarAltura(String at){
        if(!at.isEmpty()){
            return true;
        }else{
            return false;
        }
    }
}
